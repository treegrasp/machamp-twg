machamp-twg
===========

Tree Wrapping Grammar parsing with MaChAmp.

Installation
------------

Step 0: clone this repository and cd into it.

    git clone git@gitlab.com:treegrasp/machamp-twg.git
    cd machamp-twg

Step 1: update Git submodules. From this directory (`machamp-twg`):

    git submodule update --init --recursive

Step 2: switch to a dedicated virtual environment. For example, with PyEnv:

    pyenv virtualenv 3.10.8 machamp-twg
    pyenv local machamp-twg

Step 3: install PyTorch (1.12.1, MaChAmp does not support newer versions).

    pip3 install torch==1.12.1+cu116 --extra-index-url https://download.pytorch.org/whl/cu116

Step 4: install other MaChAmp dependencies

    pip3 install -r machamp/requirements.txt

Step 5: install machamp-twg dependencies

    pip3 install -r requirments.txt

Step 6: install disco-dop

    cd disco-dop
    pip install -r requirments.txt
    make discodop-venv
    cd ..

Step 7: install partage-twg (if you haven't already)

    cd partage-twg
    stack install
    cd ..

Step 8: create a logs directory on a large disk and link to it. For example:

    mkdir -p /data/$(whoami)/logs/machamp-twg
    ln -s /data/$(whoami)/logs/machamp-twg logs

Usage
-----

For example, to replicate the experiments on English from Bladier et al. (2022):

    produce experiments/rrgparbank-en/base/{dev,test}.eval

The produced `.eval` files will contain detailed scores.

To run your own experiment (let's call it `aug1`) with modified
training data (in a `train.supertags` file):

    produce experiments/rrgparbank-en/aug1/{dev,test}.eval

In order for this to work, you will have to create the following files
yourself first:

* `config/machamp/rrgparbank-en-aug1.json`. This can be the same as
  `config/machamp/rrgparbank-en-base.json` but with all occurrences of `base`
  replaced by `aug1`.
* `experiments/rrgparbank-en/aug1/train.supertags` (your modified training
  data)

References
----------

Tatiana Bladier, Kilian Evang, Valeria Generalova, Zahra Ghane, Laura
Kallmeyer, Robin Möllemann, Natalia Moors, Rainer Osswald, and Simon Petitjean.
2022. [RRGparbank: A Parallel Role and Reference Grammar
Treebank](https://aclanthology.org/2022.lrec-1.517/). In Proceedings of the
Thirteenth Language Resources and Evaluation Conference, pages 4833–4841,
Marseille, France. European Language Resources Association.
