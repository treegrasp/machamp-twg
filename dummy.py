#!/usr/bin/env python3


"""Replaces parse failures with dummy trees."""


import argparse
import re
import sys

from discodop.treebank import escape

import blocks


def stag2pos(stag):
    match = re.search(r'([^(]+) <>', stag)
    return match.group(1)


def make_dummy_tree(block):
    assert block[-1].strip() == ''
    block = tuple(l for l in block[:-1])
    tokens = tuple(l.split('\t')[1] for l in block)
    stags = tuple(l.split('\t')[3].rsplit(':', 1)[0] for l in block)
    poss = tuple(stag2pos(s) for s in stags)
    return '(SENTENCE ' + ' '.join(f'({p} {escape(t)})' for t, p in zip(tokens, poss)) + ')'


if __name__ == '__main__':
    arg_parser = argparse.ArgumentParser(description=__doc__)
    arg_parser.add_argument('supertags', help='.supertags file with distributions')
    args = arg_parser.parse_args()
    with open(args.supertags) as f:
        for i, (line, block) in enumerate(zip(sys.stdin, blocks.read(f)), start=1):
            if line.startswith('# NO PARSE '):
                print(f'WARNING: no parse for sentence #{i}, replacing with dummy tree', file=sys.stderr)
                print(make_dummy_tree(block))
            else:
                print(line, end='')
