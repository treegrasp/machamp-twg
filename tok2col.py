#!/usr/bin/env python3


import sys


if __name__ == '__main__':
    for line in sys.stdin:
        tokens = line.split()
        for i, token in enumerate(tokens, start=1):
            print(f'{i}\t{token}\t{i - 1}\t--')
        print()
