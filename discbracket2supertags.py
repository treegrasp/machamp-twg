#!/usr/bin/env python3


"""Performs TWG induction on a treebank in .discbracket format
"""


import argparse
import sys


from discodop.treebank import DiscBracketCorpusReader


sys.path.append('twg-extraction')
from twg_grammar_induction.lauras_decrossing_algorithm import \
        decross_branches, test_if_has_crossing
from twg_grammar_induction.mark_pred_nodes_wrapt_wrapb import \
        mark_wrapt
from twg_grammar_induction.twg_main import extract_twg_supertags


if __name__ == '__main__':
    arg_parser = argparse.ArgumentParser(description=__doc__)
    arg_parser.add_argument('input', help='.discbracket file')
    arg_parser.add_argument('output', help='.supertags file')
    arg_parser.add_argument('--noprobs', action='store_true',
            help='suppress :1.0 suffix (output just the tag, not a probability distribution')
    args = arg_parser.parse_args()
    tb = DiscBracketCorpusReader(args.input)
    succeeded = 0
    failed = 0
    with open(args.output, 'w') as f:
        for key, item in tb.itertrees():
            tree = item.tree
            sent = item.sent
            try:
                tree = decross_branches(tree, sent)
                test_if_has_crossing(tree)
                tree_with_wrapt_wrapb = mark_wrapt(tree, sent)
                supertags = tuple(extract_twg_supertags(tree, sent, key))
                if len(supertags) > 0:
                    for stag_arc_tuples in supertags:
                        line = str(stag_arc_tuples[2][0]) + '\t' \
                                + stag_arc_tuples[0] + '\t' \
                                + str(stag_arc_tuples[2][1]) \
                                + ('' if args.noprobs else ':1.0') \
                                + '\t' \
                                + stag_arc_tuples[1].replace('_@REF_1@', '') \
                                + ('' if args.noprobs else ':1.0')
                        line = line.replace('_@REF_2_C@', '')
                        line = line.replace('_@REF_2@', '')
                        print(line, file=f)
                    print(file=f)
                else:
                    raise ValueError()
                succeeded += 1
            except:
                print(f'WARNING: supertag extraction failed for sentence {key}, '
                        'skipping')
                failed += 1
    print(f'{succeeded} trees decomposed, {failed} failed', file=sys.stderr)
