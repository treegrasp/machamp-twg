from typing import Iterable, TextIO, Tuple


def read(f: TextIO) -> Iterable[Tuple[str, ...]]:
    current_block = []
    for line in f:
        current_block.append(line)
        if line.strip() == '':
            yield tuple(current_block)
            current_block = []
    if current_block:
        yield tuple(current_block)
