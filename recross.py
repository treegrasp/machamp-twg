#!/usr/bin/env python3


"""Postprocesses partage-twg output (recrossing etc.)
"""


import argparse
import re
import sys

sys.path.append('twg-extraction/backtransformation')
from backtransformation import conv

from discodop.tree import writediscbrackettree
from discodop.treebank import incrementaltreereader, writeexporttree


if __name__ == '__main__':
    arg_parser = argparse.ArgumentParser(description=__doc__)
    args = arg_parser.parse_args()
    for i, line in enumerate(sys.stdin, start=1):
        for t, s, comment in incrementaltreereader(line):
            orig_tree = t.copy(True).freeze()
            orig_sent = s.copy()
            try:
                recrossed_tree = conv(t, s)
                # remove any remaining decrossing annotations
                for subtree in recrossed_tree.subtrees():
                    subtree.label = re.sub(r'\[PS=[^]]+\]$', '', subtree.label)
                # check validity
                writeexporttree(recrossed_tree, orig_sent, i, '', None)
                # write recrossed tree
                print(writediscbrackettree(recrossed_tree, orig_sent), end='')
            except:
                print(f'WARNING: recrossing failed for tree #{i}, leaving decrossed', file=sys.stderr)
                # remove any remaining decrossing annotations
                for subtree in orig_tree.subtrees():
                    subtree.label = re.sub(r'\[PS=[^]]+\]$', '', subtree.label)
                print(writediscbrackettree(orig_tree, orig_sent), end='')
