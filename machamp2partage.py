#!/usr/bin/env python3


"""Convert MaChAmp output to partage-twg input."""


import fileinput
import sys


import numpy as np
from scipy.special import softmax


for line in fileinput.input():
    line = line.rstrip()
    if line:
        num, word, headdist, tagdist = line.split('\t')
        headdist = headdist.split('|')
        heads, scores = zip(*(p.split(':') for p in headdist))
        scores = softmax(np.fromiter((float(s) for s in scores), dtype=float))
        headdist = '|'.join(f'{h}:{s}' for h, s in zip(heads, scores))
        tagdist = tagdist.split('|')
        tags, scores = zip(*(p.split(':') for p in tagdist))
        scores = softmax(np.fromiter((float(s) for s in scores), dtype=float))
        tagdist = '\t'.join(f'{h}:{s}' for h, s in zip(tags, scores))
        line = f'{num}\t{word}\t{headdist}\t{tagdist}'
    print(line)
